import { LOG_IN, LOG_OUT } from "./types";

export const LogIn = {
    type: LOG_IN
}

export const LogOut = {
    type: LOG_OUT
}