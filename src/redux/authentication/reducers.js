import { LOG_IN, LOG_OUT } from "./types";

// initial state
const initialState = {
    isAuth: false,
}

// craete reducer
const authReducer = (state = initialState, action) => {
    switch(action.type) {
        case LOG_IN :
            return {
                ...state,
                isAuth: true,
            }
        case LOG_OUT :
            return {
                ...state,
                isAuth: false,
            }
        default:
            return state
    }
}

export default authReducer;