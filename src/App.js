// import './App.css';
import { BrowserRouter, Switch, Route } from 'react-router-dom';
import Home from './pages/Home';
import Content from './pages/Content';
import Navbar from './component/navbar';
import LoginButton from './component/loginButton';

function App() {
  return (
    <BrowserRouter>
      <Navbar />
      <Switch>
        <Route exact path='/' component={Home} />
        <Route path='/content' component={Content} />
      </Switch>
      <LoginButton />
    </BrowserRouter>
  );
}

export default App;
